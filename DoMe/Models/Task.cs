﻿using System;
namespace DoMe.Models
{
    public class Task
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }

    }
}
