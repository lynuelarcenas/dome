﻿using System;
using Xamarin.Forms;

namespace DoMe
{
    public class BtnClicked : TriggerAction<Button>
    {
        protected override void Invoke(Button button)
        {
            button.IsVisible = false;
            button.IsEnabled = false;
        }

    }
}
