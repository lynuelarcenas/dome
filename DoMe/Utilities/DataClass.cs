﻿using System;
namespace DoMe.Utilities
{
    public class DataClass
    {
        public static DataClass dataClass;
        public static DataClass GetInstance
        {
            get
            {
                if (dataClass == null)
                {
                    dataClass = new DataClass();
                }

                return dataClass;
            }
        }

        public int ID { get; set; }
        public string Name { get; set; }
    }

}
