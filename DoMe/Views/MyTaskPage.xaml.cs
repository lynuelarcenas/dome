﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class MyTaskPage : RootViewPage
    {
        public MyTaskPage()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void InitializeNavigation()
        {
            this.PageTitle = "MY TASK";
            this.TitleFontColor = Color.White;
            this.LeftIcon = "back";
            this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());
        }
    }
}
