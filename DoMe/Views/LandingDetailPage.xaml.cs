﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class LandingDetailPage : RootViewPage
    {
        public LandingDetailPage()
        {
            InitializeComponent();
            InitializeNavigation();
        }
        private void InitializeNavigation()
        {
            this.LeftIcon = "burger";
            this.PageTitle = "Landing";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);
        }
    }
}
