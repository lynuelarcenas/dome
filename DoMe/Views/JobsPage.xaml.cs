﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class JobsPage : RootViewPage
    {
         public JobsPage()
        {
            
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);

            JobsList.ItemsSource = TaskPage.JobCollection;

        }
        private void InitializeNavigation()
        {
            this.PageTitle = "JOBS";
            this.TitleFontColor = Color.White;
            this.LeftIcon = "burger";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);
        }
    }
}
