﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DoMe.Models;
using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class TasksPage : RootViewPage
    {
        ObservableCollection<Task> TaskCollection = new ObservableCollection<Task>();
        public TasksPage()
        {
            
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);

            TaskCollection.Add(new Task { Title = "Hugas Plato", Address = "Cebu City", Image = "BackArrow.png" });
            TaskCollection.Add(new Task { Title = "Silhig Salog", Address = "Cagayan de Oro City", Image = "BackArrow.png" });
            TaskCollection.Add(new Task { Title = "Limpyo CR", Address = "Cebu City", Image = "BackArrow.png" });
            TaskCollection.Add(new Task { Title = "Laba Sanina", Address = "Cagayan de Oro City", Image = "BackArrow.png" });
            TaskCollection.Add(new Task { Title = "Lungag Kanon", Address = "Cebu City", Image = "BackArrow.png" });

            TasksListView.ItemsSource = TaskCollection;

        }
        private void InitializeNavigation()
        {
            this.PageTitle = "TASKS";
            this.TitleFontColor = Color.White;
            this.LeftIcon = "burger";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);
        }

        void Task_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Task task = (Task)((ListView)sender).SelectedItem;
            Navigation.PushAsync(new TaskPage(task));
        }
    }
}
