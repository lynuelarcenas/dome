﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DoMe.Models;
using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class TaskPage : ContentPage
    {
        public static ObservableCollection<Task> JobCollection = new ObservableCollection<Task>();

        public TaskPage(Task taskList)
        {
            InitializeComponent();
            BindingContext = taskList;
        }

        void TaskAccepted_Clicked(object sender, System.EventArgs e)
        {
            
            Task taskAccepted = new Task() { Title="", Address="", Image="" };
            JobCollection.Add(taskAccepted);
            DisplayAlert("Accepted", "The task is now in your job list", "Okay");
            Navigation.PushAsync(new JobsPage());
        }
    }
}
