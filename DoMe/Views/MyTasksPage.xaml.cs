﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class MyTasksPage : RootViewPage
    {
        public MyTasksPage()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        private void InitializeNavigation()
        {
            this.PageTitle = "MY TASKS";
            this.TitleFontColor = Color.White;

            this.LeftIcon = "burger";
            this.LeftButtonCommand = new Command((obj) => ((MasterDetailPage)((NavigationPage)App.Current.MainPage).CurrentPage).IsPresented = true);

            this.RightIcon2 = "add";
            this.RightButton2Command = new Command((obj) => Navigation.PushAsync(new NewTaskPage()));
        }
    }
}
