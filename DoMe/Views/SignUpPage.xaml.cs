﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class SignUpPage : RootViewPage
    {
        public SignUpPage()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);


        }
        private void InitializeNavigation()
        {
            this.PageTitle = "SIGN UP";
            this.TitleFontColor = Color.White;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new SignInPage());
        }
    }
}
