﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class NewTaskPage : RootViewPage
    {
        public NewTaskPage()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);


        }
        private void InitializeNavigation()
        {
            this.PageTitle = "NEW";
            this.TitleFontColor = Color.White;

            this.LeftIcon = "back";
            this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());

        }
    }
}
