﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class LandingPage : MasterDetailPage
    {
        public LandingPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }


        void Tasks_Clicked(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new TasksPage());
            this.IsPresented = false;
        }


        void Jobs_Clicked(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new JobsPage());
            this.IsPresented = false;
        }

        void MyTasks_Clicked(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new MyTasksPage());
            this.IsPresented = false;
        }
    }
}
