﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class RootViewPage : ContentPage
    {
        public static readonly BindableProperty PageTitleProperty = BindableProperty.Create("PageTitle", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty TitleFontFamilyProperty = BindableProperty.Create("TitleFontFamily", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty TitleFontColorProperty = BindableProperty.Create("TitleFontColor", typeof(Color), typeof(RootViewPage), Color.Black);
        public static readonly BindableProperty LeftIconProperty = BindableProperty.Create("LeftIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIcon1Property = BindableProperty.Create("RightIcon1", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIcon2Property = BindableProperty.Create("RightIcon2", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create("LeftButtonCommand", typeof(ICommand), typeof(RootViewPage), null);
        public static readonly BindableProperty RightButton1CommandProperty = BindableProperty.Create("RightButton1Command", typeof(ICommand), typeof(RootViewPage), null);
        public static readonly BindableProperty RightButton2CommandProperty = BindableProperty.Create("RightButton2Command", typeof(ICommand), typeof(RootViewPage), null);

        public string PageTitle
        {
            set { SetValue(PageTitleProperty, value); }
            get { return (string)GetValue(PageTitleProperty); }
        }

        public string TitleFontFamily
        {
            set { SetValue(TitleFontFamilyProperty, value); }
            get { return (string)GetValue(TitleFontFamilyProperty); }
        }

        public Color TitleFontColor
        {
            set { SetValue(TitleFontColorProperty, value); }
            get { return (Color)GetValue(TitleFontColorProperty); }
        }

        public string LeftIcon
        {
            set { SetValue(LeftIconProperty, value); }
            get { return (string)GetValue(LeftIconProperty); }
        }

        public string RightIcon1
        {
            set { SetValue(RightIcon1Property, value); }
            get { return (string)GetValue(RightIcon1Property); }
        }

        public string RightIcon2
        {
            set { SetValue(RightIcon2Property, value); }
            get { return (string)GetValue(RightIcon2Property); }
        }

        public ICommand LeftButtonCommand
        {
            set { SetValue(LeftButtonCommandProperty, value); }
            get { return (ICommand)GetValue(LeftButtonCommandProperty); }
        }

        public ICommand RightButton1Command
        {
            set { SetValue(RightButton1CommandProperty, value); }
            get { return (ICommand)GetValue(RightButton1CommandProperty); }
        }

        public ICommand RightButton2Command
        {
            set { SetValue(RightButton2CommandProperty, value); }
            get { return (ICommand)GetValue(RightButton2CommandProperty); }
        }


        public RootViewPage()
        {
            InitializeComponent();
        }
    }
}
