﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoMe.Views
{
    public partial class SignInPage : RootViewPage
    {
        public SignInPage()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        private void InitializeNavigation()
        {
            this.PageTitle = "SIGN IN";
            this.TitleFontColor = Color.White;
        }
    }
}
